pragma solidity ^0.8.0;

contract AuctionPlatform {

    struct Auction {
        uint auctionID;
        string description;
        uint startTime;
        uint endTime;
        uint minBidValue;
        address auctionOwner;
        bool closed;
    }

    struct Bid {
        uint auctionID;
        uint bidValue;
        address bidder;
    }

    mapping (uint => Auction) public auctions;
    mapping (uint => Bid[]) public bids;
    mapping (address => uint[]) public bidderAuctions;

    uint public auctionCounter;

    // 1. Create an Auction {AuctionID ,Description , Start Time , End Time , MinBidValue}
    function createAuction(string memory description, uint startTime, uint endTime, uint minBidValue) public {
        require(startTime >= block.timestamp, "Auction start time must be in future");
        require(endTime > startTime, "Auction end time must be after start time");
        require(minBidValue > 0, "Minimum bid value must be greater than zero");

        auctionCounter++;
        auctions[auctionCounter] = Auction(auctionCounter, description, startTime, endTime, minBidValue, msg.sender, false);
    }

    // Place Bids On Active Auctions {AuctionID , BidValue} 
    function placeBid(uint auctionID, uint bidValue) public {
        require(block.timestamp >= auctions[auctionID].startTime, "Auction not started yet");
        require(block.timestamp <= auctions[auctionID].endTime, "Auction closed");
        
        bids[auctionID].push(Bid(auctionID, bidValue, msg.sender));
    }

    // Bid Owners Can see list of all Auctions where they placed their Bids.
    function getBidsForAuction(uint auctionID) public view returns (Bid[] memory) {
        require(msg.sender == auctions[auctionID].auctionOwner, "Only auction owner can view");

        return bids[auctionID];
    }

    // Bid Owners Can see list of all Auctions where they placed their Bids.
    function getAuctionsForBidder() public view returns (uint[] memory) {
        return bidderAuctions[msg.sender];
    }

    // Auction Owner can select a bid and Mark the Auction status as Closed.
    function closeAuction(uint auctionID, uint _bidIndex) public {
        require(msg.sender == auctions[auctionID].auctionOwner, "Only auction owner can close auction");
        require(bids[auctionID].length > _bidIndex, "Invalid bid index");
        require(!auctions[auctionID].closed, "Auction already closed");

        auctions[auctionID].closed = true;

        // Transfer bid amount to auction owner
        payable(auctions[auctionID].auctionOwner).transfer(bids[auctionID][_bidIndex].bidValue);
    }

    // All Other Necessary View Functions.
    function getAuctionDetails(uint auctionID) public view returns (Auction memory) {
        return auctions[auctionID];
    }
}
