pragma solidity ^0.8.0;

// Note : this is not a working solution 
// ToDo:  1. Transfer sender amount to Painting owner
// ToDo: 2. Transfer ownership


contract PaintingsNFT {

    struct Painting {
        string name;
        string artist;
        uint256 price;
        bool sold;
    }

    Painting[] public paintings;

    uint256 public nextPaintingId = 1;
    uint256 public commissionFeePercentage = 10;

    event PaintingListed(uint256 indexed paintingId, string name, string artist, uint256 price);
    event PaintingBought(uint256 indexed paintingId, string name, string artist, uint256 price, address buyer);


    function listPainting(string memory name, string memory artist, uint256 price) public {
        require(price > 0, "Price must be greater than 0");

        paintings.push(Painting(name, artist, price, false));
        uint256 paintingId = nextPaintingId;
        nextPaintingId++;

        emit PaintingListed(paintingId, name, artist, price);

    }

    function buyPainting(uint256 paintingId) public payable {
        Painting storage painting = paintings[paintingId - 1];
        require(!painting.sold, "Painting already sold");
        require(msg.value == painting.price, "Incorrect payment amount");

        painting.sold = true;

   
        uint256 paymentAmount = msg.value;


        emit PaintingBought(paintingId, painting.name, painting.artist, painting.price, msg.sender);
    }

   
    // Function to transfer Ether from this contract to address from input
    function transfer(address payable _to, uint _amount) public {
        // Note that "to" is declared as payable
        (bool success, ) = _to.call{value: _amount}("");
        require(success, "Failed to send Ether");
    }

    function getPaintingsCount() public view returns (uint256) {
        return paintings.length;
    }

    function getPaintingDetails(uint256 paintingId) public view returns (Painting memory) {
        require(paintingId > 0 && paintingId <= paintings.length, "Invalid painting ID");
        return paintings[paintingId - 1];
    }
}
